<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 4:27 PM
 */
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$con = new mysqli($servername, $username, $password);
$con->select_db('cvphil');

// Check connection
if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}?>