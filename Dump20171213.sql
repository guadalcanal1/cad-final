-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cvphil
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `text` text,
  `published_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES (1,'We Bought a Tuba','In a bold move, we took the offensive against our critics by acquiring a tuba, which is pretty much the biggest instrument we\'ve ever had. This is a huge step in the right direction for us.','2017-12-10 20:33:18'),(2,'We Sold Our Tuba','It just wasn\'t working out. The man who sold it to us turned out to be a drug addict, and he hid all of his drugs inside the tuba. Not only did it muffle the noise, but our tubaist was forced to go to the ER.','2017-12-11 20:33:18'),(3,'We Have Been Sponsored','My brother is sponsoring us now. More details to follow, but I can already say this is gonna be huge.','2017-12-12 20:33:18');
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performances`
--

DROP TABLE IF EXISTS `performances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `performances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opus` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performances`
--

LOCK TABLES `performances` WRITE;
/*!40000 ALTER TABLE `performances` DISABLE KEYS */;
INSERT INTO `performances` VALUES (1,'52','All-Night Vigil','Choral','2018-02-28 23:00:00'),(2,'79','Andante and Finale','','2018-04-04 18:00:00'),(3,'8','Capriccio','','2018-02-02 18:00:00'),(4,'39','Children\'s Album','','2018-08-07 18:00:00'),(5,'56','Concert Fantasia','Concert','2018-10-31 20:00:00'),(6,'59','Dumka ','','2018-01-28 18:00:00'),(7,'72','Eighteen Pieces ','','2018-05-20 18:00:00'),(8,'77','Fatum ','','2018-09-09 18:00:00'),(9,'15','Festival Overture on the Danish National Anthem ','','2018-11-02 15:00:00'),(10,'32','Francesca da Rimini ','','2018-03-15 18:00:00'),(11,'37','Grand Sonata ','','2018-06-06 18:00:00'),(12,'67a','Hamlet—incidental music ','','2018-09-01 18:00:00'),(13,'67','Hamlet—overture-fantasia ','','2018-05-09 18:00:00'),(14,'69','Iolanta ','Opera','2018-08-08 20:00:00'),(15,'45','Italian Capriccio ','','2018-03-30 18:00:00'),(16,'41','Liturgy of Saint John Chrysostom ','','2018-03-04 18:00:00'),(17,'58','Manfred ','Symphony','2018-09-30 16:00:00'),(18,'62','Pezzo capriccioso ','','2018-01-30 14:00:00'),(19,'23','Piano Concerto No. 1','Concert','2018-03-28 17:00:00'),(20,'44','Piano Concerto No. 2','Concert','2018-02-19 18:00:00'),(21,'75','Piano Concerto No. 3 ','Concert','2018-09-10 19:00:00'),(22,'80','Piano Sonata in C-sharp minor ','','2018-07-08 18:00:00'),(23,'50','Piano Trio','','2018-07-30 18:00:00'),(24,'5','Romance ','','2018-07-28 18:00:00'),(25,'1','Scherzo à la russe  and Impromptu','','2018-06-18 18:00:00'),(26,'48','Serenade for String Orchestra ','','2018-11-28 18:00:00'),(27,'26','Sérénade mélancolique ','','2018-06-13 18:00:00'),(28,'47','Seven Romances ','','2018-07-07 19:00:00'),(29,'46','Six Duets ','','2018-11-05 18:00:00'),(30,'65','Six French Songs ','','2018-09-11 18:00:00'),(31,'19','Six Pieces ','','2018-11-16 16:00:00'),(32,'21','Six Pieces on a Single Theme ','','2018-05-24 18:00:00'),(33,'6','Six Romances ','','2018-05-11 18:00:00'),(34,'27','Six Romances and Songs ','','2018-07-11 18:00:00'),(35,'54','Sixteen Songs for Children','','2018-09-22 18:00:00'),(36,'31','Slavonic March ','','2018-08-22 18:00:00'),(37,'70','Souvenir de Florence ','','2018-10-14 18:00:00'),(38,'2','Souvenir de Hapsal ','','2018-08-04 18:00:00'),(39,'42','Souvenir d\'un lieu cher ','','2018-03-17 18:00:00'),(40,'11','String Quartet No. 1 ','','2018-11-04 18:00:00'),(41,'22','String Quartet No. 2 ','','2018-11-14 18:00:00'),(42,'30','String Quartet No. 3 ','','2018-12-09 18:00:00'),(43,'43','Suite No. 1','','2018-08-27 18:00:00'),(44,'53','Suite No. 2 ','','2018-03-13 18:00:00'),(45,'55','Suite No. 3 ','','2018-07-12 18:00:00'),(46,'61','Suite No. 4 ','','2018-04-12 18:00:00'),(47,'20','Swan Lake','Ballet','2018-01-12 17:00:00'),(48,'13','Symphony No. 1','Symphony','2018-01-31 18:00:00'),(49,'17','Symphony No. 2 ','Symphony','2018-03-31 18:00:00'),(50,'29','Symphony No. 3 ','Symphony','2018-05-31 18:00:00'),(51,'36','Symphony No. 4 ','Symphony','2018-07-31 18:00:00'),(52,'64','Symphony No. 5 ','Symphony','2018-08-31 18:00:00'),(53,'74','Symphony No. 6 ','Symphony','2018-12-31 18:00:00'),(54,'71','The Nutcracker—ballet','Ballet','2018-12-12 17:00:00'),(55,'71a','The Nutcracker—suite ','','2018-12-24 16:00:00'),(56,'68','The Queen of Spades ','Opera','2018-06-20 12:00:00'),(57,'37a','The Seasons ','','2018-05-29 13:00:00'),(58,'66','The Sleeping Beauty','Ballet','2018-05-07 16:30:00'),(59,'12','The Snow Maiden ','','2018-12-30 19:00:00'),(60,'76','The Storm ','','2018-04-04 16:00:00'),(61,'18','The Tempest ','','2018-11-01 14:00:00'),(62,'78','The Voyeovda—symphonic ballad','','2018-05-10 21:00:00'),(63,'3','The Voyevoda—opera','Opera','2018-10-10 15:00:00'),(64,'49','The Year 1812 ','','2018-07-04 17:00:00'),(65,'9','Three Pieces ','','2018-03-01 15:00:00'),(66,'40','Twelve Pieces ','','2018-12-01 11:00:00'),(67,'60','Twelve Romances ','','2018-02-14 18:00:00'),(68,'10','Two Pieces','','2018-08-28 20:00:00'),(69,'14','Vakula the Smith ','Opera','2018-01-15 18:00:00'),(70,'4','Valse-Caprice ','','2018-04-30 15:00:00'),(71,'7','Valse-Scherzo ','','2018-05-21 19:00:00'),(72,'33','Variations on a Rococo Theme','Concert','2018-06-01 15:00:00'),(73,'35','Violin Concerto ','Concert','2018-09-14 17:00:00'),(74,'24','Yevgeny Onegin','Opera','2018-03-30 15:30:00');
/*!40000 ALTER TABLE `performances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(75) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` VALUES (1,'Test','Nugget'),(2,'garberjd@masters.edu','Josh VVVVVVV Boi');
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-13  1:16:03
