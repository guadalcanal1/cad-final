<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:30 PM
 */
include('frag/head.php');
include('frag/header.php');
?>
<div class="shadow"></div>
<div class="headerImage" style="background-image: url(/img/orchestra1.jpg);">
    <h1>Events</h1>
</div>
<div class="container" style="padding: 10px">
    <div id="calendar"></div>
</div>
<script src='/fc/lib/jquery.min.js'></script>
<script src='/fc/lib/jquery-ui.min.js'></script>
<script src='/fc/lib/moment.min.js'></script>
<script src='/fc/fullcalendar.min.js'></script>
<script>
    $('#calendar').fullCalendar({
        async: false,
        events:{
            url: './listEvents.php'
        },
        eventAfterRender: function( event, element, view ){
            var opus = event.opus;
            var type = event.type;
            element.append('<p style="margin-bottom: 2px">Op. ' + opus + '</p>');
            if(type)
                element.append('<p style="margin-bottom: 2px; font-weight: bold;">' + type + '</p>');
        }
    })
</script>
<?php include('frag/footer.php');?>
