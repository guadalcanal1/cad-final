<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:30 PM
 */
include('frag/head.php');
include('frag/header.php');
?>
<div class="shadow"></div>
<div class="headerImage" style="background-image: url(/img/orchestra2.png);">
    <h1>Notes</h1>
</div>
<section class="section--padding-bottom-small" style="padding-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="post">
                    <div class="image"><a href="blogPost.php"><img src="img/blog4.jpg" alt="" class="img-responsive"></a></div>
                    <h3><a href="blogPost.php">You won't believe what we did!</a></h3>
                    <p class="post__intro">ellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    <p class="read-more"><a href="blogPost.php" class="btn btn-ghost">Continue reading   </a></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="post">
                    <div class="image"><a href="blogPost.php"><img src="img/blog5.jpg" alt="" class="img-responsive"></a></div>
                    <h3><a href="blogPost.php">28 Crazy things about cymbals. You won't believe #4!!!</a></h3>
                    <p class="post__intro"> Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
                    <p class="read-more"><a href="blogPost.php" class="btn btn-ghost">Continue reading   </a></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="post">
                    <div class="image"><a href="blogPost.php"><img src="img/blog1.jpg" alt="" class="img-responsive"></a></div>
                    <h3><a href="blogPost.php">How many people can we fit in a tuba?</a></h3>
                    <p class="post__intro">ellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    <p class="read-more"><a href="blogPost.php" class="btn btn-ghost">Continue reading     </a></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="post">
                    <div class="image"><a href="blogPost.php"><img src="img/blog2.jpg" alt="" class="img-responsive"></a></div>
                    <h3><a href="blogPost.php">Does it damage a grand piano to drop it? Today we find out!</a></h3>
                    <p class="post__intro"> Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
                    <p class="read-more"><a href="blogPost.php" class="btn btn-ghost">Continue reading     </a></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="post">
                    <div class="image"><a href="blogPost.php"><img src="img/blog3.jpg" alt="" class="img-responsive"></a></div>
                    <h3><a href="blogPost.php">Check out this story about how we ate a flute!</a></h3>
                    <p class="post__intro">ellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    <p class="read-more"><a href="blogPost.php" class="btn btn-ghost">Continue reading     </a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('frag/footer.php');?>
