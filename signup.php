<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 9:59 PM
 */

include('frag/head.php');
include('frag/header.php');
?>
    <div class="shadow"></div>
    <div class="headerImage" style="background-image: url(/img/orchestra3.jpg);">
        <h1>Register for the Newsletter</h1>
    </div>
    <section class="signup">
        <div class="container" style="text-align: center">
            <h2>You can get exclusive access to our events!</h2>
            <form action="submitEmail.php" method="POST" id="signupForm">
                <label for="email">Email: </label><input type="email" name="email" id="email"><br/>
                <label for="name">Your Name: </label><input type="text" name="name" id="name"><br/>
                <input type="submit" value="Sign Me Up">
            </form>
        </div>
    </section>
<?php include('frag/footer.php');?>