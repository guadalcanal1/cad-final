<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 10:03 PM
 */
include_once('connection.php');
$email = $_POST['email'];
$name = $_POST['name'];
$stmt = $con->prepare("INSERT INTO subscribers (email, name) VALUES (?, ?)");
$stmt->bind_param("ss", $email, $name);
$stmt->execute();
header("location: signedUp.php");