<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 4:27 PM
 */
include_once('connection.php');
$results = $con->query('select * from `performances`');
$result = '[';
while($row = mysqli_fetch_row($results)){
    $opus = $row[1];
    $title = $row[2];
    $type = $row[3];
    $date = $row[4];
    $result .= '{"title": "' . $title . '", "type": "' . $type . '", "allDay": 0, "start": "' . $date . '", "opus": "' . $opus . '", "end": "' . $date . '"},';
}
$result = substr($result, 0, strlen($result) - 1);
$result .= "]";
echo $result;
?>

