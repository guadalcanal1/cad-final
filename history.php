<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:30 PM
 */
include('frag/head.php');
include('frag/header.php');
?>
<div class="shadow"></div>
<div class="headerImage" style="background-image: url(/img/orchestra2.png);">
    <h1>History</h1>
</div>
<section class="blog-post">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="post-content margin-bottom--big">
                  <p>Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you. It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life… He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself.</p>
                </div>
                <!-- /.post-content-->
            </div>
        </div>
    </div>
</section>
<?php include('frag/footer.php');?>
