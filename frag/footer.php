<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:28 PM
 */?>
<footer class="footer">
    <div class="footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a href="https://htmlhigh5.com" class="external"><p>&copy;2017 HTML High 5</p></a>
                    <div class="footerSocial">
                        <a href="https://facebook.com"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="credit">Template by <a href="https://bootstrapious.com/free-templates" class="external">Bootstrapious templates</a></p>
                    <hr/>
                    <div style="text-align: right">
                        <p>1234 Main Street | Canyon Country | 91322</p>
                        <p>email@thiswebsite.com</p>
                        <p>818.555.1234</p>
                    </div>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Javascript files-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.cookie.js"> </script>
<script src="../js/lightbox.min.js"></script>
<script src="../js/front.js"></script><!-- substitute:livereload -->
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
<!---->
</body>
</html>
