<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:30 PM
 */
include('frag/head.php');
include('frag/header.php');
?>
<div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-home" data-slide-to="1"></li>
        <li data-target="#carousel-home" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides-->
    <div role="listbox" class="carousel-inner">
        <div style="background-image: url('img/orchestra1.jpg');" class="item active">
            <div class="overlay"></div>
            <div class="carousel-caption">
                <h1 class="super-heading">Canyon Valley Philharmonic</h1>
                <p class="super-paragraph">The greatest orchestra since  <a href="https://htmlhigh5.com">htmlhigh5.com</a>.</p>
            </div>
        </div>
        <div style="background-image: url('img/orchestra2.png');" class="item">
            <div class="overlay"></div>
            <div class="carousel-caption">
                <h1 class="super-heading">We have the most songs</h1>
                <p class="super-paragraph">More than most singers</p>
            </div>
        </div>
        <div style="background-image: url('img/orchestra3.jpg');" class="item">
            <div class="overlay"></div>
            <div class="carousel-caption">
                <h1 class="super-heading">Our music is louder than your music</h1>
                <p class="super-paragraph">We know you think you're special. Guess what? You aren't.</p>
            </div>
        </div>
    </div>
</div>
<div class="homeContent col-md-12" style="padding: 20px; background-color: white; height: auto;">
    <h2>Performances</h2>
    <div class="col-md-12" >
        <div class="col-md-9">
            <div id="calendar"></div>
        </div>
        <div class="col-md-3">
            <a class="twitter-timeline" href="https://twitter.com/cvphilharmonic?ref_src=twsrc%5Etfw">Tweets by cvphilharmonic</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>
    <div class="col-md-12">
        <h2>News & Announcements</h2>
        <?php include('news.php') ?>
    </div>
</div>
<script src='/fc/lib/jquery.min.js'></script>
<script src='/fc/lib/jquery-ui.min.js'></script>
<script src='/fc/lib/moment.min.js'></script>
<script src='/fc/fullcalendar.min.js'></script>
<script>
    $('#calendar').fullCalendar({
        async: false,
        events:{
            url: './listEvents.php'
        },
        eventAfterRender: function( event, element, view ){
            var opus = event.opus;
            var type = event.type;
            element.append('<p style="margin-bottom: 2px">Op. ' + opus + '</p>');
            if(type)
                element.append('<p style="margin-bottom: 2px; font-weight: bold;">' + type + '</p>');
        }
    })
</script>
<?php include('frag/footer.php');?>
