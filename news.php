<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 8:34 PM
 */
if(!isset($con))
    include('connection.php');
$results = $con->query('select * from `announcements` order by `published_at` desc limit 3');
while($row = mysqli_fetch_row($results)){
    $title = $row[1];
    $text = $row[2];
    $date = $row[3];
    echo "<div class='col-md-4 announcement'>";
    echo "<h3>$title</h3>";
    echo "<p>" . substr($text, 0, 250) . "</p>";
    echo "<small>" . date("F jS, Y", strtotime($date)) . "</small>";
    echo "</div>";
}
?>