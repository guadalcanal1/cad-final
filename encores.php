<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:30 PM
 */
include('frag/head.php');
include('frag/header.php');
?>
<div class="shadow"></div>
<div class="headerImage" style="background-image: url(/img/orchestra2.png);">
    <h1>Encores</h1>
</div>
<section class="blog-post">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="post-content margin-bottom--big">
                    <a href="https://htmlhigh5.com" target="_blank"><h2 style="color: orangered; font-size: 72px; font-weight: bold;">HTML High 5</h2></a>
                    <p style="font-size: 1.1em"><a href="https://htmlhigh5.com" target="_blank">HTMLHigh5.com</a> is a
                         website I made this summer which contains some of the finest games on the internet, all of which
                    happen to be made by me. You can create an account, save your scores, earn trophies, and top the leaderboards.
                    It's definitely worth taking a look at!!!</p>
                    <a href="https://htmlhigh5.com" target="_blank">
                        <div style="text-align: center">
                            <img alt="HTML High 5" src="img/htmlhigh5_screenshot.png" style="width: 600px; max-width: 100%; margin: 0 auto;">
                        </div>
                    </a>
                    <hr/>
                </div>
                <div class="post-content margin-bottom--big">
                    <h2>55 Minute Games</h2>
                    <p>I made a couple HTML5 games while I was bored in math class this year. You can play them:</p>
                    <a href="bored.html" target="_blank"><h3 style="color: #5cb85c">Bored 1</h3></a>
                    <a href="bored2.html" target="_blank"><h3 style="color: #5cb85c">Bored 2</h3></a>
                    <hr/>
                </div>
                <div class="post-content margin-bottom--big">
                    <h2>PHP Coding Assignments from this Semester</h2>
                    <p>If you'd like, you may download some of the PHP code I wrote this semester in class.</p>
                    <ul>
                        <li><a href="./hw9.zip" download="hw9.zip">Homework 9</a></li>
                        <li><a href="./goblins.zip" download="goblins.zip">Goblins</a></li>
                    </ul>
                </div>
                <!-- /.post-content-->
            </div>
        </div>
    </div>
</section>
<?php include('frag/footer.php');?>
