<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 9:59 PM
 */

include('frag/head.php');
include('frag/header.php');
?>
    <div class="shadow"></div>
    <div class="headerImage" style="background-image: url(/img/orchestra2.png);">
        <h1>You Did It</h1>
    </div>
    <section class="signup">
        <div class="container">
            <h2>You have signed up! Keep looking at our website!</h2>
        </div>
    </section>
<?php include('frag/footer.php');?>