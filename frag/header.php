<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 12/12/2017
 * Time: 2:28 PM
 */
$uri = "$_SERVER[REQUEST_URI]";
?>
<body class="home" style="height: auto; background-color: white;">
<!-- navbar-->
<header class="header">
    <div role="navigation" class="navbar navbar-default navbar-fixed-top" id="stickyNavbar">
        <div class="container">
            <div class="navbar-header"><a href="../" class="navbar-brand">CV Phil</a>
                <div class="navbar-buttons">
                    <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
                </div>
            </div>
            <div id="navigation" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <?php
//                    printf("<li class='%s'><a href='../'>Home</a></li>", $uri == '/' || $uri == '/index.php' ? 'active' : 'inactive');
                    printf("<li class='%s'><a href='../notes.php'>Notes</a></li>", $uri == '/notes.php' ? 'active' : 'inactive');
                    printf("<li class='%s'><a href='../events.php'>Events</a></li>", $uri == '/events.php' ? 'active' : 'inactive');
                    printf("<li class='%s'><a href='../signup.php'>Join</a></li>", $uri == '/signup.php' ? 'active' : 'inactive');
                    printf("<li class='%s'><a href='../encores.php'>Encores</a></li>", $uri == '/encores.php' ? 'active' : 'inactive');
                    ?>
                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="../history.php">Our History</a></li>
                            <li><a href="../mission.php">Our Mission</a></li>
                        </ul>
                    </li>
                    <li class="hidden-sm hidden-xs"><a href="https://htmlhigh5.com" target="_blank" style="color: orangered">HTML High 5</a></li>
                </ul>
                <div class="headerSocial">
                    <a href="https://facebook.com/htmlhigh5"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/cvphilharmonic"><i class="fa fa-twitter"></i></a>
                    <a href="https://plus.google.com/+HTMLHighFive"><i class="fa fa-google-plus"></i></a>
                    <span style="color: white">| 818.555.1234</span>
                </div>
            </div>
        </div>
    </div>
</header>
